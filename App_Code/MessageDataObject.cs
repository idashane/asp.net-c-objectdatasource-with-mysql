﻿using System;
using System.Data;
using System.ComponentModel;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Package.MySQLDB;



namespace Package.MessageDataObject
{
    [DataObject(true)]
    public static class MessageDataObject
    {      

        [DataObjectMethod(DataObjectMethodType.Select)]
        public static List<MessageList> GetMessage()
        {
            MySQLDB.MySQLDB oDB = new MySQLDB.MySQLDB();
            List<MessageList> oLists = new List<MessageList>();
            

            string sSQL = "SELECT ";
            sSQL += " messages.message_id, ";
            sSQL += " messages.campaign_id, ";
            sSQL += " campaigns.campaign_name,  ";
            sSQL += " messages.subject, ";
            sSQL += " messages.template_name ";
            sSQL += " FROM ";
            sSQL += " messages ";
            sSQL += " inner join campaigns on campaigns.campaign_id=messages.campaign_id";
            DataTable oTbl = new DataTable();
            oTbl = oDB.GetTable(sSQL);

            if (oTbl.Rows.Count > 0)
            {
                int iInt = 0;

                for (iInt = 0; iInt < oTbl.Rows.Count; iInt++)
                {
                    MessageList oList = new MessageList();
                    oList.MessageID = Convert.ToInt32(oTbl.Rows[iInt]["message_id"].ToString());
                    oList.CampaignName = oTbl.Rows[iInt]["campaign_name"].ToString();
                    oList.Subject = oTbl.Rows[iInt]["subject"].ToString();
                    oList.TemplateName = oTbl.Rows[iInt]["template_name"].ToString();

                    oLists.Add(oList);
                    oList = null;
                }
            }


            oDB = null;
            return oLists;
            
        }

        [DataObjectMethod(DataObjectMethodType.Update)]
        public static void UpdateMessage(MessageList oList)
        {
            MySQLDB.MySQLDB oDB = new MySQLDB.MySQLDB();            
            string sSQL = "update messages set subject=\""+ oList.Subject.ToString() +"\" where message_id=\""+ oList.MessageID.ToString() +"\"";
            oDB.Amened(sSQL);           

            
        }
    }   

}
