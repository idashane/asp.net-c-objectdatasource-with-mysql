﻿using System;

public class MessageList
{
    int iMessageID = 0;
    string sCampaignName = "";
    string sSubject = "";
    string sTemplateName = "";

    public int MessageID
    {
        get { return iMessageID; }
        set { iMessageID = value; }
    }

    public string CampaignName
    {
        get { return sCampaignName; }
        set { sCampaignName = value; }
    }


    public string Subject
    {
        get { return sSubject; }
        set { sSubject = value; }
    }

    public string TemplateName
    {
        get { return sTemplateName; }
        set { sTemplateName = value; }
    }

}


