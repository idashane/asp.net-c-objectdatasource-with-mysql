﻿using System;
using System.Data;
using MySql.Data.MySqlClient;


namespace Package.MySQLDB{
    public class MySQLDB
    {

        string sConn = "Server=MailDB;Database=Mail2;Uid=ceta_admin;Pwd=rL4drK0c;Connection Timeout=10;default command timeout=10;Allow Zero Datetime=True;";
        
        public DataTable GetTable(string sSQL)
        {
            try
            {
                MySqlConnection sConnection = new MySqlConnection(sConn);
                sConnection.Open();
                MySqlDataAdapter dAdapter = new MySqlDataAdapter(sSQL, sConnection);
                DataTable dTable = new DataTable();
                dAdapter.Fill(dTable);


                sConnection.Close();
                sConnection = null;
                dAdapter = null;

                return dTable;
            }
            catch (Exception ex)
            {
                throw new System.Exception("Function : MySQLDB.GetTable - " + ex.Message);
            }
        }

        public void Amened(string sSQL)
        {
            try
            {
                using (MySqlConnection oConnection = new MySqlConnection(sConn))
                {
                    MySqlCommand oCommand = new MySqlCommand(sSQL, oConnection);                  
                    oConnection.Open();
                    oCommand.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception("Function : MySQLDB.MySQLUpdate - " + ex.Message);
            }
        }

    }
}
